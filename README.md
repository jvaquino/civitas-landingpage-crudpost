*Clone o repositório* ↓↓↓

https://gitlab.com/jvaquino/civitas-landingpage-crudpost.git

*Entre na pasta do projeto* ↓↓↓

cd civitas-landingpage-crudpost

*Abra um novo terminal pelo VSCode e execute o comando* ↓↓↓

npm install

*Execute o comando funcional para iniciar a API* ↓↓↓

npx json-server --watch db.json

*Abra o arquivo index.html para executar a API*
