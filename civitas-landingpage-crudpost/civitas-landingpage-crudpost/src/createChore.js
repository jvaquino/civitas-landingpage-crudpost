const inputNome = document.querySelector("#input-nome");
const inputEmail = document.querySelector("#input-email");
const inputData = document.querySelector("#input-data");
const inputSenha = document.querySelector("#input-senha");
const createBtn = document.querySelector("#submit-button");
const errSpan = document.querySelector(".error");

createBtn.addEventListener("click", async (event) => {
    event.preventDefault();

const nome = inputNome.value;
const email = inputEmail.value;
const data = inputData.value;
const senha = inputSenha.value;
  
    if (nome === "" || email === ""|| data === ""|| senha === "") {
      errSpan.classList.remove("invisible");
    } else {
      errSpan.classList.add("invisible");
      await postChores(nome, email,data,senha)
     
    }
  });

const postChores = async (nome, email, data, senha) => {
  try {
    const response = await fetch("http://localhost:3000/posts", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nome: `${nome}`,
        email: `${email}`,
        data: `${data}`,
        senha: `${senha}`,
      }),
    });

    const content = await response.json();

    return content;
  } catch (error) {
    console.log(error);
  }
};
